//Creamos un nuevo controller para hacer el login y el logout
const io = require('../io');
const crypt = require("../crypt");
const requestJson = require("request-json");
const baseMLabURL = "https://api.mlab.com/api/1/databases/proyectofinal/collections/";
const mLabAPIKey = "apiKey=" + process.env.mLabAPIKey;
 // practica 2 login


function loginUsersPr(req, res) {
 console.log("POST /apitechu/proyecto/login");
 console.log("APIKEy " + mLabAPIKey);
  var query = 'q={"email": "' + req.body.email + '"}';
  console.log("query es " + query);
  console.log("reqbodyyyy" + req.body.id);
 httpClient = requestJson.createClient(baseMLabURL);
 console.log("URL:" + baseMLabURL + "user?" + query + "&" + mLabAPIKey);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (err)
     console.log("500");
      //devolver el 500
     else {
       if (body.length > 0) {
         var isPasswordcorrect = true;// crypt.checkPassword(req.body.password, body[0].password);
         console.log("Password correct is " + isPasswordcorrect);
         if (!isPasswordcorrect) {
           var response = {
             "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
           }
           res.status(401);
           res.send(response);
         } else {

           ///user?q={"id" :'+body[0].id +'}&{"$set":{"logged":true}} &apiKey=myAPIKey

           console.log("Got a user with that email and password, logging in");
           query = 'q={"id" : ' + body[0].id +'}';
           console.log("Query for put is " + query);
           var putBody = '{"$set":{"logged":true}}';
           console.log("URL PUT:" + baseMLabURL + "user?" + query + "&" + mLabAPIKey);
           httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
             function(errPUT, resMLabPUT, bodyPUT) {
               console.log("PUT realizado: Se graba el logged a true");
               var response = {
                 "msg" : "Usuario logado con éxito",
                 "idUsuario" : body[0].id,
                 "Nombre" : body[0].first_name,
                 "Apellido": body[0].second_name,
                 "Genero" : body[0].gender,
                 }
               res.send(response);

             }
           )
         }
       }
       else {
           console.log("PUT done else" );
       }
     }
//aqui termina el else

   }
 );
}


function logoutUsersPr(req, res) {
 console.log("POST /apitechu/proyecto/logout/:id");

 var query = 'q={"id": ' + req.params.id + '}';
 console.log("query es " + query);

 httpClient = requestJson.createClient(baseMLabURL);
 httpClient.get("user?" + query + "&" + mLabAPIKey,
   function(err, resMLab, body) {
     if (body.length == 0) {
       var response = {
         "mensaje" : "Logout incorrecto, usuario no encontrado"
       }
       res.send(response);
     } else {
       if (body[0].logged=="false"){
       var response = {
         "mensaje" : "usuario no estaba logado"
       }
       }else
       console.log("Got a user with that id, logging out");
        console.log("Got xcvxcva " + body[0].logged);
       query = 'q={"id" : ' + body[0].id +'}';
       console.log("Query for put is " + query);
       var putBody = '{"$unset":{"logged":""}}'
       httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
         function(errPUT, resMLabPUT, bodyPUT) {
           console.log("PUT done");
           var response = {
             "msg" : "Usuario deslogado",
             "idUsuario" : body[0].id
           }
           res.send(response);
         }
       )
     }
   }
 );
}

module.exports.loginUsersPr = loginUsersPr ;
module.exports.logoutUsersPr = logoutUsersPr ;
